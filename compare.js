import { performance } from "perf_hooks";
import mersenneTwister from "./ice_modules/mersenne-twister.js";
import { MersenneTwister } from "./mersenne.js";

var mt = new mersenneTwister( [ 0x123, 0x234, 0x345, 0x456 ] );
var fast = MersenneTwister( [ 0x123, 0x234, 0x345, 0x456 ] );
var perf = {
	"mersenne-twister": {
		"start": null,
		"end": null
	},
	"fast-mersenne-twister": {
		"start": null,
		"end": null
	}
};

// Warmup
console.log( "Warming up mersenne-twister with 100,000 random integers" );
for( let i = 0; i < 100000; i ++ ){
	mt.random_int();
}

console.log( "1,000,000 random integers from mersenne-twister" );
perf[ "mersenne-twister" ].start = performance.now();
for( let i = 0; i < 1000000; i ++ ){
	mt.random_int();
}
perf[ "mersenne-twister" ].end = performance.now();
console.log( "✔" );

// Warmup
console.log( "Warming up fast-mersenne-twister with 100,000 random integers" );
for( let i = 0; i < 100000; i ++ ){
	fast.randomNumber();
}

console.log( "1,000,000 random integers from fast-mersenne-twister" );
perf[ "fast-mersenne-twister" ].start = performance.now();
for( let i = 0; i < 1000000; i ++ ){
	fast.randomNumber();
}
perf[ "fast-mersenne-twister" ].end = performance.now();
console.log( "✔" );

perf[ "mersenne-twister" ].total = perf[ "mersenne-twister" ].end - perf[ "mersenne-twister" ].start;
perf[ "fast-mersenne-twister" ].total = perf[ "fast-mersenne-twister" ].end - perf[ "fast-mersenne-twister" ].start;

console.table( perf );

let multi = ( perf[ "mersenne-twister" ].total / perf[ "fast-mersenne-twister" ].total ).toFixed( 2 );

console.log( "fast-mersenne-twister is", `${multi}X`, multi < 1 ? "as fast" : "faster" );
console.log( "👍🏻" );
console.log( "\n" );
