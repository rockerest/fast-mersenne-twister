import mersenneTwister from "./ice_modules/mersenne-twister.js";
import { int32, random } from "./original_data.js";

// This seed is the same as the one the original mt19937ar.c uses
var mt = new mersenneTwister( [ 0x123, 0x234, 0x345, 0x456 ] );

var mtInt32 = Array( 1000 ).fill( 0 ).map( () => mt.random_int() );
var mtRandom = Array( 1000 ).fill( 0 ).map( () => mt.random().toFixed( 8 ) );

// The original mt19937ar.c outputs numbers truncated to 8,
// but JavaScript drops trailing zeroes when it reads them in
var randomStrings = random.map( ( r ) => r.toFixed( 8 ) );

var errors = [];

console.log( "Testing mersenne-twister against the original output." );

int32.forEach( ( orig32, i ) => {
	if( orig32 != mtInt32[ i ] ){
		errors.push( `INT ${( "000" + ( i + 1 ) ).slice( -4 )}: Computed ${mtInt32[ i ]}, should have gotten ${orig32}.` );
	}
} );

randomStrings.forEach( ( origRandom, i ) => {
	if( origRandom != mtRandom[ i ] ){
		errors.push( `RND ${i + 1001}: Computed ${mtRandom[ i ]}, should have gotten ${origRandom}.` );
	}
} );

if( errors.length ){
	errors.forEach( ( e ) => {
		console.error( e );
	} );

	throw new Error( "Didn't compare cleanly." );
}
else{
	console.log( "✅" );
}
