# fast-mersenne-twister

A fast Mersenne Twister

## Usage

```javascript
import { MersenneTwister } from "fast-mersenne-twister";

var myFastTwister = MersenneTwister( 1234567890 );

console.log( myFastTwister.random() ); // 0.6187947695143521
```

You can also use an array seed:

```javascript
import { MersenneTwister } from "fast-mersenne-twister";

var myFastTwister = MersenneTwister( [ 1234, 5678, 9012 ] );

console.log( myFastTwister.random() ); // 0.22977210697717965
```

### Methods

All of the original methods are available on the MersenneTwister object returned by the exported function.  

They are also aliased to more readable / convenient names.  

| Convenience | Original | Return |
| ----------- | -------- | ------ |
| `randomNumber` | `genrand_int32` | 32 bit integer, [0,0xffffffff] |
| `random31Bit` | `genrand_int31` | 31 bit integer, [0,0x7fffffff] |
| `randomInclusive` | `genrand_real1` | float, [0,1] |
| `random` | `genrand_real2` | float, [0,1) (this is just like what `Math.random()` returns) |
| `randomExclusive` | `genrand_real3` | float, (0,1) |
| `random53Bit` | `genrand_res53` | float, [0,1) with 53-bit resolution |

## Testing

You can recompile the original program:

```console
g++ mt19927.c -o mt
```

If you run it with `./mt` it will output 1000 32bit integers and 1000 numbers [0,1).  
I've removed the original 5-column output to make formatting `original_data.js` a little easier.

----

You can test this implementation against the data in `original_data.js` with:

```console
node ./test.js
```

or

```console
npm run test
```

----

I found this faster implementation by Stephan Brumme and updated it because I thought the most widely used implementation - `mersenne-twister` (which is also used internally by libraries like ChanceJS) - wasn't fast enough.

I suspect that implementation has also made its way into a lot of other software just by the fact that it's the thing that comes up when you search for "JavaScript Mersenne Twister."

If you'd like to see how much faster this implementation is, run:

```console
npm install
npm run pack
npm run compare
```

(`fast-mersenne-twister` consistently comes in around 2X `mersenne-twister`, usually ~2.2)

----

For all of the possible output from this library, run:

```console
npm install
npm run pack
npm run all-meta
```

## Notes

- Extremely true to the [original algorithm by Matsumoto & Nishimura](http://www.math.sci.hiroshima-u.ac.jp/~m-mat/MT/MT2002/emt19937ar.html)
- Converted to JavaScript by [Stephan Brumme](https://create.stephan-brumme.com/mersenne-twister/)
    - This version cannot be initialized with an array
- Updated
    - with my code style
	- with all of the original random generators
	- with the ability to be initialized with an array
	- with the correct range (the original only outputs unsigned integers, while Stephan's version outputs signed integers)

The output of this implementation matches the output of the check-file on the original website linked above.

I took the array-seed function from Sean McCullough's [gist](https://gist.github.com/banksean/300494) (the grand-daddy of all[?] the other JavaScript Mersenne Twisters).  
I modified it very slightly (removed the UInt cast to be deferred to the Int32 function)
